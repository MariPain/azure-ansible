# Azure & Ansible

### Référentiels : Administrateur Cloud 

**Contexte du projet**

Notre client Simplon souhaite mettre en place un site web dans azure avec une base de données.
Nous allons réaliser le déploiement de manière automatique avec ansible.

**Cahier des charges** :

​Vous devez utiliser le document Azure_infra qui vous donnera les informations sur l'infra à mettre en place. (voir lien)

Dans un premier temps, vous allez mettre en place votre infrastructure dans azure

Config infra : (respecter les conventions de nommage défini dans Azure_infra)

- **​groupes de ressources**
- **réseau et sous réseau**
- **Groupe de sécurité NSG** à définir (identique pour le réseau)
- **Type de VM** : `Standard D2s v3` (`Ansible-Master`) / `Standard B1s` (`Node-web` et `Node-db`)
- `Linux Deb12`
- **Connexion** *ssh* par clé (une seule clé, identique pour toutes nos machines)
- **subnet admin** : `Ansible-Master`
- **subnet 01** : `Node-web`
- **subnet 02** : `Node-db`

**Etape 1: Création de l'infrastructure Azure**

Utilisez le document Azure_infra pour obtenir les informations sur l'infrastructure à mettre en place.
Créez vos groupes de ressources selon les informations fournies dans le document.

```
Nome:NOME 
prenome: PRENOME
grupo de recursos: OCC_ASD_Prenome
nombre de vnet: Vnet_OCC_ASD_Prenome
plage: /24
nombre de subnet: Subnet_Vnet_OCC_ASD_Prenome
plage: /28
subnet admin:  10.0.16.0/28 -10.0.16.15/28
soit: 16  sous réseaux

```
Créez votre réseau et vos sous-réseaux conformément aux spécifications
Définissez votre groupe de sécurité (NSG) en respectant les conventions de nommage.
Notez: Attendez la validation avant de passer à l'étape suivante.

**Etape 2: Création des machines virtuelles**

Créez la VM `Ansible-Master` avec une adresse `IP publique`.
Assurez-vous de pouvoir vous connecter en`SSH` à la VM `Ansible-Master` en utilisant *MobaXterm* ou le shell Windows.


**come se conecter?**
....



Installez les paquets nécessaires au bon fonctionnement d'Ansible sur la VM `Ansible-Master`.


```
    sudo apt update
    sudo apt install ansible
    ansible --version
    
```

Vérifiez le bon fonctionnement d'Ansible. 

Installez git

```
sudo apt install git
```

Créez les VM `Node-web` et `Node-db`.

Assurez-vous de pouvoir vous connecter en SSH depuis `Ansible-Master` sur les VM `Node-web` et `Node-db`.

**Accès à les MV avec MobaXterm**

**1. Accès à la machine master-ansible avec MobaXterm**

Ouvrez MobaXterm sur votre ordinateur local.
Créez une nouvelle session SSH avec l'adresse IP publique de la machine master-ansible.
Dans la configuration de la session, spécifiez la clé privée fournie par Azure pour vous authentifier.

**2. Connexion à node-web depuis master-ansible :**

Une fois connecté à master-ansible, vérifiez le chemin de la clé privée. Par exemple, supposons que la clé privée soit dans ~/.ssh/id_rsa.

Pour copier la clé depuis l'hôte (votre PC) vers master-ansible, vous avez deux options :

**a)** Vous pouvez copier et coller la même clé en utilisant le presse-papiers de MobaXterm et des éditeurs de texte comme nano.

**b)** Utilisez la commande scp depuis master-ansible pour copier la clé privée vers node-web :

Maintenant, depuis master-ansible, vous pouvez vous connecter aux machines virtuelles node-web et node-db en utilisant SSH. Par exemple :


 ```
 scp ~/.ssh/id_rsa nodeweb@adresse_ip_node_web:~/.ssh/
 ```

Entrez le mot de passe de nodeweb lorsqu'on vous le demande. Ainsi, master-ansible dispose de la clé privée qui lui permettra d'accéder aux machines qui la partagent.


bash
Copy code
ssh utilisateur@ip_node_web
bash
Copy code
ssh utilisateur@ip_node_db
    
**Étape 3: Apprentissage d'Ansible**

Maintenant que nous avons installé Ansible sur `Ansible-Master`, nous allons travailler avec lui dans une série de TPs. Pour cela, nous allons effectuer quelques configurations basiques préalables :

**Démarrage de l'automatisation avec Ansible**
1. Création de l'inventaire Ansible

```
mkdir ~/ansible_quickstart
```

2. Construction d'un inventaire

Créez un fichier nommé `inventory.ini` dans le répertoire `ansible_quickstart` que vous avez créé lors de l'étape précédente.

```
touch inventory.ini
```

Ajoutez un nouveau groupe [myhosts] au fichier `inventory.ini`

```
nano inventory.ini
```

```
[myhosts]

[hosts]
10.0.16.132 ansible_user=nodeweb
10.0.16.133 ansible_user=nodedb

[ansible_host]
10.0.16.4
```

*Il est conseillé d'ajouter l'adresse IP de l'hôte Ansible.*


**TP:**
*Accedez a [TP](./TP)
pour apprendre et comprendre le fonctionnement d'Ansible.*


**Etape 4: Provisioning des machines Node avec Ansible**

Mettez en place le provisioning des machines Node (Node-web et Node-db) à l'aide de la VM Ansible-Master.
Structurez les dossiers et les fichiers conformément aux instructions fournies.
Utilisez un playbook.yml pour la configuration.
Adaptez l'exemple de serveur LAMP fourni aux besoins du projet.
Assurez-vous que toute la configuration est réalisée avec Ansible. 

