<?php
    $greetings = array(
        'en' => 'Hello World!',
        'es' => '¡Hola Mundo!',
        'fr' => 'Bonjour le monde!'
    );

    $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    $greeting = isset($greetings[$language]) ? $greetings[$language] : $greetings['en'];

    echo $greeting;
?>
