**Affichez l'espace disponible sur chaque nœud (machine) dans l'inventaire.**

```  
ansible all -i inventory.ini -m shell -a 'df -h'
```

Cette commande exécute la commande `df -h` sur tous les nœuds d'inventaire définis dans le fichier `inventory.ini`. La commande `df -h` affiche l'espace disponible sur le système de fichiers dans un format lisible par l'homme.

```bash
master@Ansible-Master:~/ansible_quickstart$ ansible all -i inventory.ini -m shell -a 'df -h'
10.0.16.4 | CHANGED | rc=0 >>
Filesystem      Size  Used Avail Use% Mounted on
udev            3.9G     0  3.9G   0% /dev
tmpfs           796M  544K  796M   1% /run
/dev/sdb1        30G  1.5G   27G   6% /
tmpfs           3.9G  124K  3.9G   1% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
/dev/sdb15      124M   12M  113M  10% /boot/efi
/dev/sda1        16G   24K   15G   1% /mnt
tmpfs           796M     0  796M   0% /run/user/1000
10.0.16.132 | CHANGED | rc=0 >>
Filesystem      Size  Used Avail Use% Mounted on
udev            445M     0  445M   0% /dev
tmpfs            92M  492K   91M   1% /run
/dev/sdb1        30G  697M   28G   3% /
tmpfs           456M     0  456M   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
/dev/sdb15      124M   12M  113M  10% /boot/efi
/dev/sda1       3.9G   24K  3.7G   1% /mnt
tmpfs            92M     0   92M   0% /run/user/1000
10.0.16.133 | CHANGED | rc=0 >>
Filesystem      Size  Used Avail Use% Mounted on
udev            445M     0  445M   0% /dev
tmpfs            92M  492K   91M   1% /run
/dev/sda1        30G  697M   28G   3% /
tmpfs           456M     0  456M   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
/dev/sda15      124M   12M  113M  10% /boot/efi
/dev/sdb1       3.9G   24K  3.7G   1% /mnt
tmpfs            92M     0   92M   0% /run/user/1000

```