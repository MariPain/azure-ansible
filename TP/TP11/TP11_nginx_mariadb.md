**Installez Nginx et Mariabd** 

TP11 Installez Nginx sur la machine Node-web et mariadb sur la machine Node-db

**Note importante :** En raison de la modification des noms d'hôtes par le playbook, nous utiliserons désormais inventory_hostname au lieu de group_names pour les scripts faisant référence à la nomenclature originale de l'inventaire.

Lorsque les noms d'hôtes sont modifiés à l'aide d'un playbook, les nouveaux noms d'hôtes seront reflétés dans inventory_hostname, qui représente le nom d'hôte tel qu'il est défini dans l'inventaire. Cela garantit que les scripts et les playbooks continuent à fonctionner correctement même après la modification des noms d'hôtes, car ils utilisent les informations d'inventaire d'origine. L'utilisation de inventory_hostname au lieu de group_names permet une plus grande précision et cohérence dans l'exécution des tâches et garantit que les actions sont effectuées sur les hôtes corrects selon la configuration définie dans l'inventaire.

```bash 
master@Ansible-Master:~/ansible_quickstart$ ansible-playbook -i inventory.ini playbook_install_ngnix_mariadb.yaml

PLAY [Install Nginx and MariaDB] *****************************************************

TASK [Gathering Facts] ***************************************************************
ok: [10.0.16.132]
ok: [10.0.16.133]

TASK [Update apt repositories] *******************************************************
changed: [10.0.16.132]
changed: [10.0.16.133]

TASK [Set Nginx software info on node-web] *******************************************
skipping: [10.0.16.132]
skipping: [10.0.16.133]

TASK [Ensure Nginx is enabled and running] *******************************************
skipping: [10.0.16.132]
skipping: [10.0.16.133]

TASK [Check Nginx status on node-web] ************************************************
skipping: [10.0.16.132]
skipping: [10.0.16.133]

TASK [Display Nginx status on node-web] **********************************************
skipping: [10.0.16.132]
skipping: [10.0.16.133]

TASK [Set MariaDB software info on node-db] ******************************************
skipping: [10.0.16.132]
skipping: [10.0.16.133]

TASK [Ensure MariaDB is enabled and running] *****************************************
skipping: [10.0.16.132]
skipping: [10.0.16.133]

TASK [Check MariaDB status on node-db] ***********************************************
skipping: [10.0.16.132]
skipping: [10.0.16.133]

TASK [Display MariaDB status on node-db] *********************************************
skipping: [10.0.16.132]
skipping: [10.0.16.133]

TASK [Display Installation Message Nginx] ********************************************
ok: [10.0.16.132] => {
    "msg": "Le logiciel Nginx sera installé et activé sur 10.0.16.132."
}
ok: [10.0.16.133] => {
    "msg": "Le logiciel Nginx sera installé et activé sur 10.0.16.133."
}

TASK [Display Installation Message MariaDB] ******************************************
ok: [10.0.16.132] => {
    "msg": "Le logiciel MariaDB sera installé et activé sur 10.0.16.132."
}
ok: [10.0.16.133] => {
    "msg": "Le logiciel MariaDB sera installé et activé sur 10.0.16.133."
}

PLAY RECAP ***************************************************************************
10.0.16.132                : ok=4    changed=1    unreachable=0    failed=0    skipped=8    rescued=0    ignored=0
10.0.16.133                : ok=4    changed=1    unreachable=0    failed=0    skipped=8    rescued=0    ignored=0

```
