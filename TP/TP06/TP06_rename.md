**Playbook pour renommer vos machines**

Tout au long de ce TP, je me suis rendu compte que j'exécutais des tâches pour les sudousers, ce qui nécessite d'inclure l'utilisateur ansible dans la liste des utilisateurs sudo. à ce stade, j'ai deux idées en tête :

1. à quel point l'ajout au fichier sudo group user peut être fastidieux.
2. La faille de sécurité que représente le fait qu'un utilisateur se connecte à distance et contrôle une infrastructure.

Dans ce `playbook.yaml` je vais maintenant essayer de créer des playbooks plus efficaces et plus sûrs qui exécutent des tâches spécifiques avec des permissions sans avoir à inclure ansibleuser dans le groupe sudo de chaque machine.


Nous utilisons le module de `commande` pour exécuter la commande `hostnamectl set-hostname` afin de modifier le nom de l'hôte. En outre, nous utilisons les options become : yes` et become_user : root` pour exécuter les commandes avec les privilèges de l'utilisateur root.


Il est pratique de revenir sur les playbooks d'arrêt et de redémarrage et de les refaire afin de spécifier l'exécution des tâches sans inclure asibleuser dans le groupe sudo pour protéger les machines virtuelles.

