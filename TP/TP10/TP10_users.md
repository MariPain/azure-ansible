**Créer des utilisateurs en boucle.**

Créer un playbook play-users.yml qui créera 3
utilisateurs. Ces trois utilisateurs appartiennent au groupe users.


```bash
 master@Ansible-Master:~/ansible_quickstart$ ansible-playbook -i inventory.ini playbook_create_users.yaml

PLAY [Create users] *********************************************************************

TASK [Gathering Facts] ******************************************************************
ok: [10.0.16.132]
ok: [10.0.16.133]

TASK [Crear usuarios en bucle] **********************************************************
changed: [10.0.16.132] => (item=user1)
changed: [10.0.16.133] => (item=user1)
changed: [10.0.16.132] => (item=user2)
changed: [10.0.16.133] => (item=user2)
changed: [10.0.16.132] => (item=user3)
changed: [10.0.16.133] => (item=user3)

TASK [Creation msg] *********************************************************************
ok: [10.0.16.132] => {
    "msg": "Des utilisateurs ont été créés"
}
ok: [10.0.16.133] => {
    "msg": "Des utilisateurs ont été créés"
}

PLAY RECAP ******************************************************************************
10.0.16.132                : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
10.0.16.133                : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

```