**Playbook pour éteindre les machines virtuelles (VM)**

**Considérations préliminaires**

Afin de pouvoir exécuter un livre qui arrête les machines avec la commande shutdown, l'utilisateur ansible doit avoir les permissions sudo sur les hôtes et pour cela je dois modifier le fichier sudo visudo.

Pour permettre à Ansible d'exécuter la commande `poweroff` sans demander de mot de passe sur chaque hôte distant :

1. accéder au fichier `/etc/sudoers` sur chaque hôte.
```
sudo visudo
```
2. Ajoutez la ligne `ansible_user ALL=(ALL) NOPASSWD: /sbin/shutdown`
` à la fin du fichier.
3. enregistrez les modifications et fermez le fichier.


Une fois les permissions accordées pour exécuter le playbook, tapez la commande 
```
ansible-playbook -i inventory.ini playbook_shutdown.yaml

```

- name: shutdown hosts
  hosts: myhosts
  tasks:
    - name: Print message
      ansible.builtin.debug:
       msg: "Bye"
    - name: Shutdown hosts
      become: true
      command: /sbin/shutdown -h now




```bash
 master@Ansible-Master:~/ansible_quickstart$ ansible-playbook -i inventory.ini playbook_shutdown.yaml

PLAY [shutdown hosts] **********************************************************

TASK [Gathering Facts] *********************************************************
ok: [10.0.16.132]
ok: [10.0.16.133]

TASK [Print message] ***********************************************************
ok: [10.0.16.132] => {
    "msg": "Bye"
}
ok: [10.0.16.133] => {
    "msg": "Bye"
}

TASK [Shutdown hosts] **********************************************************
changed: [10.0.16.132]
changed: [10.0.16.133]

PLAY RECAP *********************************************************************
10.0.16.132                : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
10.0.16.133                : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```
