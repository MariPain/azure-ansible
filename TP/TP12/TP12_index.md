**Modifier la page index.html**

Une fois l'installation effectuée, utilisez
le playbook pour modifier la page index.html.

```bash
master@Ansible-Master:~/ansible_quickstart$ ansible-playbook -i inventory.ini playbook_index_nodeweb.yaml

PLAY [Modification index.html fichier] ***********************************************

TASK [Gathering Facts] ***************************************************************
ok: [10.0.16.132]
ok: [10.0.16.133]

TASK [Update index.html en node-web] *************************************************
skipping: [10.0.16.133]
ok: [10.0.16.132]

TASK [Mostrar la modificación] *******************************************************
ok: [10.0.16.132] => {
    "msg": "Index.html file it was create and modify on node-web."
}
skipping: [10.0.16.133]

PLAY RECAP ***************************************************************************
10.0.16.132                : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
10.0.16.133                : ok=1    changed=0    unreachable=0    failed=0    skipped=2    rescued=0    ignored=0

master@Ansible-Master:~/ansible_quickstart$ ssh nodeweb@10.0.16.132                   Linux node-web 6.1.0-18-cloud-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.76-1 (2024-02-01) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Tue Mar 12 10:56:13 2024 from 10.0.16.4
nodeweb@node-web:~$ cat /var/www/html/index.html
<h1>Hello world by Ansible</h1>nodeweb@node-web:~$

```