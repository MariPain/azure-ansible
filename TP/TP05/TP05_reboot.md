**Playbook pour redémarrer les machines virtuelles (VM)**

**Considérations préliminaires**

Répétez la tâche d'attribution des droits sudo de la dernière tâche du TP 4, cette fois dans la commande `reboot`.

1. accéder au fichier `/etc/sudoers` sur chaque hôte.
```
sudo visudo
```
2. Ajoutez la ligne `ansible_user ALL=(ALL) NOPASSWD: /sbin/reboot`
` à la fin du fichier.
3. enregistrez les modifications et fermez le fichier.


Une fois les permissions accordées pour exécuter le playbook, tapez la commande 
```
ansible-playbook -i inventory.ini playbook_reboot.yaml

```

```bash
- name: reboot hosts
  hosts: myhosts
  tasks:
    - name: Print message
      ansible.builtin.debug:
       msg: "see you now"
    - name: reboot hosts
      become: true
      command: /sbin/reboot

```