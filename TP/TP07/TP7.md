**Variable globale**


Playbook pour afficher une variable globale Le playbook permet d'afficher les noms des
distributions des cibles ainsi que leurs versions majeures en utilisant des variables
globales.



master@Ansible-Master:~/ansible_quickstart$ ansible-playbook -i inventory.ini playbook_dst.yaml

PLAY [Display the names of target distributions] *********************************************************************************************************

TASK [Gathering Facts] ***********************************************************************************************************************************
ok: [10.0.16.133]
ok: [10.0.16.132]

TASK [Display target distributions] **********************************************************************************************************************
ok: [10.0.16.132] => {
    "msg": "Debian 12.5"
}
ok: [10.0.16.133] => {
    "msg": "Debian 12.5"
}

PLAY RECAP ***********************************************************************************************************************************************
10.0.16.132                : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
10.0.16.133                : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0


