**Utilisation d’une variable locale de type dictionnaire.**


Utiliser cette variable dans un message : Le logiciel apache sera installé par le paquet httpd.



```bash
master@Ansible-Master:~/ansible_quickstart$ nano playbook_apache_install_var.yaml        master@Ansible-Master:~/ansible_quickstart$ ansible-playbook -i inventory.ini playbook_apache_install_var.yaml

PLAY [Apache install] *******************************************************************

TASK [Gathering Facts] ******************************************************************
ok: [10.0.16.132]
ok: [10.0.16.133]

TASK [Set Apache software info] *********************************************************
ok: [10.0.16.132]
ok: [10.0.16.133]

TASK [Display Installation Message] *****************************************************
ok: [10.0.16.132] => {
    "msg": "Le logiciel apache sera installé par le paquet httpd."
}
ok: [10.0.16.133] => {
    "msg": "Le logiciel apache sera installé par le paquet httpd."
}

PLAY RECAP ******************************************************************************
10.0.16.132                : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
10.0.16.133                : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

```