**Effectuer un ping vers toutes les machines avec Ansible**

```
ansible myhosts -m ping -i inventory.ini
```

Le résultat devrait ressembler à ceci:

```bash

master@Ansible-Master:~/ansible_quickstart$ ansible myhosts -m ping -i inventory.ini
ansible-inventory -i inventory.ini --list10.0.16.133 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
10.0.16.132 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}

```