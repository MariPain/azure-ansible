**Variable Locale**

Créer une variable locale nommée port_http contenant la valeur 8080. Utiliser
cette variable dans un message : Le port 8080 sera utilisé


```bash

master@Ansible-Master:~/ansible_quickstart$ ansible-playbook -i inventory.ini playbook_tp08.yaml

PLAY [create local var for http:port] ************************************************

TASK [Gathering Facts] ***************************************************************
ok: [10.0.16.133]
ok: [10.0.16.132]

TASK [Set http_port variable to 8080] ************************************************
ok: [10.0.16.132]
ok: [10.0.16.133]

TASK [Display HTTP port] *************************************************************
ok: [10.0.16.132] => {
    "msg": "El puerto HTTP 8080 será utilizado"
}
ok: [10.0.16.133] => {
    "msg": "El puerto HTTP 8080 será utilizado"
}

PLAY RECAP ***************************************************************************
10.0.16.132                : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
10.0.16.133                : ok=
```