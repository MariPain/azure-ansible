**Affichez le nom d'hôte de chaque machine dans l'inventaire.**



```
ansible-inventory -i inventory.ini --list
```


```ini
{
    "_meta": {
        "hostvars": {
            "10.0.16.132": {
                "ansible_user": "nodeweb"
            },
            "10.0.16.133": {
                "ansible_user": "nodedb"
            }
        }
    },
    "all": {
        "children": [
            "ungrouped",
            "myhosts",
            "ansible_host"
        ]
    },
    "ansible_host": {
        "hosts": [
            "10.0.16.4"
        ]
    },
    "myhosts": {
        "hosts": [
            "10.0.16.132",
            "10.0.16.133"
        ]
    }
}

```


